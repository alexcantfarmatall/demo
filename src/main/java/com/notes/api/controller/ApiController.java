package com.notes.api.controller;

import java.awt.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.notes.model.Note;
import com.notes.security.NoteService;

@RestController
public class ApiController {
	@Autowired
	NoteService noteService;
	
	@GetMapping("/${userid}/notes")
	public String userNotes() {
		return "index";
	}

}
