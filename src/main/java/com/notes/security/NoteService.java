package com.notes.security;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.notes.model.Note;

@Service
public class NoteService {

	Logger logger = LoggerFactory.getLogger(NoteService.class);
	public MongoCollection<Document> mongoCollection;
	
	public NoteService() {}
	
	@Autowired
	public NoteService(MongoDatabase db) {
		System.out.println("here");
		this.mongoCollection = db.getCollection("notes");
	}
	public List<Note> getUserNotes(String userName) {
		return null;

	}
	
	public List<Note> getByTags(String tag) {
		return null;
	}
	
	public void insert(Note note) {
		mongoCollection.insertOne(new Document("tag",note.getTags()).append("content", note.getContent()));
	}

	public List<Note> findNotes(String tag) {
		List<Note> result = new ArrayList<Note>();
		FindIterable<Document> notes = mongoCollection.find(new Document("tag",tag));
		
		for(Document noteDocument : notes) {
			Note note = new Note(noteDocument.getString("tag"), noteDocument.getString("content"));
			logger.info("added: " +  note.getTags()+ " : "+ note.getContent());
		}
		
		return result;
	}
}
