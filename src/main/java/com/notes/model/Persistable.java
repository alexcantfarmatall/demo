package com.notes.model;

import java.io.Serializable;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

public abstract class Persistable implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	protected String id = ObjectId.get().toString();
	
	public String getId() {
		return this.id;
	}
	
	public void setId(final String id) {
		this.id = id;
	}
}
