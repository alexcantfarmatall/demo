package com.notes.model;

import java.io.Serializable;


public class Note implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String tag;
	private String content;
	private long timeStamp;
	


	public Note() {}
	
	public Note(String tag, String content) {
		this.tag = tag;
		this.content = content;
	}

	public String getTags() {
		return tag;
	}

	public void setTags(String tag) {
		this.tag = tag;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	public long getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}
	
	
}
