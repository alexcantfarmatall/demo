package com.notes.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;

@Configuration
public class Configurations {
	
	public Configurations() {}
	
	Logger logger = LoggerFactory.getLogger(Configurations.class);
	MongoClientURI uri = new MongoClientURI("mongodb://admin1:admin1@ds253960.mlab.com:53960/notes");
	
	@Bean
	public MongoClient mongoClient() {
		return new MongoClient(uri);
	}
	
	@Bean
	public MongoDatabase database(MongoClient mongoClient) {
		logger.info("database: " +MongoClient.class.toGenericString());
		return mongoClient.getDatabase("notes");
	}
	@Bean
	InternalResourceViewResolver internalResourceViewResolver() {
		InternalResourceViewResolver internalResourceViewResolver = new InternalResourceViewResolver();
		internalResourceViewResolver.setPrefix("/demo/src/main/resources/templates/");
		internalResourceViewResolver.setSuffix(".html");
		return internalResourceViewResolver;
	}

	
}
