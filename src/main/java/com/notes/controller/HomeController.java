package com.notes.controller;

import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.notes.model.Note;
import com.notes.security.NoteService;

@Controller
public class HomeController {
	
	Logger logger = LoggerFactory.getLogger(HomeController.class);
	@Autowired
	NoteService noteService;
	
	@PostConstruct
	public void post() {
		logger.info("post ctr");

	}
	
	@GetMapping("/")
	public String index(Model model) {
	
		logger.info("at index");
		Note note = new Note("my tag", "this is code");
		model.addAttribute("note",note.getContent());
		noteService.insert(note);
		
		
		return "index";
	}
	
	@PostMapping("/search")
	public String search(@RequestParam String tag, Model model) {
		logger.info("@ search mapping");
		if(tag.trim().length() > 0){
			List<Note> notesMatchedTag = noteService.findNotes(tag);
			model.addAttribute("notesMatchedTag", notesMatchedTag);
		}
		
		return "index";
	}

}
